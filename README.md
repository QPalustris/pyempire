# pyempire

This is a Python translation of an old BASIC game from 1986 called empire, which was itself a translation from a TRS-80 game from the 70's. Why remake in Python? Not much else to do during a blizzard in the midwest.

The Python version has an improved computer player logic which is a little more fair and matches what human players are faced with. The old "classic" behavior can be reenabled with the '-c' command line switch. The '-v' command line switch enables a new verbose mode which provides more information on what the computer players are doing.

See the readme in the basic folder for more on gameplay.
