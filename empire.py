#!/usr/bin/env python

import sys
import curses
import random
import math


class Player(object):
    def __init__(self, name, country, titles, currency, game):
        self.human = True
        self.name = name
        self.country = country
        self.titles = titles
        self.currency = currency
        self.g = game

        self.land = 10000
        self.grain = 15000 + random.randint(0, 10000)
        self.serfs = 2000
        self.money = 1000
        self.merchants = 25
        self.tax_c = 20
        self.tax_s = 5
        self.tax_i = 35
        self.soldiers = 20
        self.level = 0
        self.nobles = 1
        self.armyeff = 15  # 15=150%
        self.alive = True
        self.grainsale = 0
        self.grainprice = 0
        self.markets = 0
        self.mills = 0
        self.foundries = 0
        self.shipyards = 0
        self.palace = 0  # 1 = 5%
        self.harvest = 0  # This year's harvest
        self.starved = 0  # Number that starved this year
        self.immigrant = 0 # Number of people immigrated this year
        self.babies = 0  # These are for verbose reporting only.
        self.disease = 0
        self.malnut = 0
        self.newpeople = 0
    
    def full_name(self):
        return '{0} {1} OF {2}'.format(self.titles[self.level], self.name, self.country)
        
    def short_name(self):
        return '{0} {1}'.format(self.titles[self.level], self.name)
    
    def setupscr(self):
        ''' Set up screen for this player '''
        self.g.scr.addstr(0,0, self.full_name())
        self.g.scr.clrtoeol()
        self.g.scr.refresh()
    
    def calc_grain(self):
        ''' Calculate grain requirements '''
        # Not all land is available for planting.
        land_avail = self.land - self.serfs - self.nobles*2 - self.palace - self.merchants - self.soldiers*2
        self.harvest = max(0, land_avail * self.g.weather * 0.72 + random.random()*500 - self.foundries*500)
        rats = random.random() * 30
        self.grain = self.grain - self.grain*rats/100 + self.harvest
        
        people_need = (self.serfs + self.merchants + self.nobles*3) * 5
        army_needs  = self.soldiers * 8
        return people_need, army_needs, rats
    
    def calc_population(self, food, people, army, people_need, army_needs):
        win = new_window()
        self.armyeff = max(5, army / (army_needs+.001) * 10)
        self.armyeff = min(15, self.armyeff)

        babies = int(random.random()*people/9.5)
        disease = int(random.random()*people/22)
        immigrant = 0
        if (food > 1.5*people_need):
            d = math.sqrt(food-people_need) - (self.tax_c*self.g.weather*1.5)
            if d > 0:
                immigrant = int(random.random()*2*d+1)
        self.immigrant = immigrant
        self.babies = babies
        self.disease = disease

        malnut = 0
        self.starved = 0  # Gives a chance of assassination
        if people_need > 2*food:
            self.starved = int(random.random()*people/16+1)
            malnut  = int(random.random()*people/12+1)
        elif people_need > food:
            malnut  = int(random.random()*people/15+1)
        self.malnut = malnut
        im_merch = int(random.random()*immigrant/5)  # Some immigrants are merchants
        im_noble = int(random.random()*immigrant/25)
        self.merchants += im_merch
        self.nobles += im_noble
        new_people = babies - disease - malnut - self.starved + immigrant - im_merch - im_noble
        self.serfs += new_people
        self.newpeople = new_people

        arm_starve = 0
        if army_needs > army*2:
            arm_starve = int(random.random()*self.soldiers/2+1)
            self.soldiers -= arm_starve

        if self.human:
            win.clear()
            win.addstr(1, 0, 'In year {},'.format(self.g.year))
            line = 2
            if babies > 0:
                win.addstr(line, 1, '{} BABIES WERE BORN'.format(babies))
                line = line + 1
            if immigrant > 0:
                win.addstr(line, 1, '{} PEOPLE IMMIGRATED INTO YOUR COUNTRY'.format(immigrant))
                line = line + 1
            if disease > 0:
                win.addstr(line, 1, '{} PEOPLE DIED OF DISEASE'.format(disease))
                line = line + 1
            if malnut > 0:
                win.addstr(line, 1, '{} PEOPLE DIED OF MALNUTRITION'.format(malnut))
                line = line + 1
            if self.starved > 0:
                win.addstr(line, 1, '{} PEOPLE STARVED TO DEATH'.format(self.starved))
                line = line + 1
            if arm_starve > 0:
                win.addstr(line, 1, '{} SOLDIERS STARVED TO DEATH'.format(arm_starve))
                line = line + 1
            win.addstr(line, 0, 'YOUR ARMY WILL FIGHT AT {:.0%} EFFICIENCY'.format(self.armyeff/10))
            gain = 'LOST'
            if new_people>0: gain = 'GAINED'
            win.addstr(line+1, 0, 'YOUR POPULATION {0} {1:.0f} CITIZENS.'.format(gain, abs(new_people)))
            win.addstr(line+3, 0, '<ENTER>')
            win.getstr()

    def select_battle(self):
        ''' Human player choose battle '''
        win = new_window()
        attack_num = 1
        c = None
        while c is not '\n':
            win = new_window()
            win.addstr(1, 3, 'LAND HOLDINGS')
            win.addstr(4, 10, '1)  BARBARIANS  \t {:>9.1f}'.format(self.g.barbarians))
            for i, p in enumerate(self.g.players):
                if p.alive:
                    win.addstr(5+i, 10, '{0})  {1:<10} \t {2:>9.1f}'.format(i+2, p.country, p.land))

            win.addstr(15, 1, 'WHO DO YOU WISH TO ATTACK (GIVE #)? ')
            c = win.getkey()
            if not c.isdigit() or int(c)<1 or int(c)>7: continue
            if c == '1':
                pdef = 'barb'
            else:
                pdef = self.g.players[int(c)-2]
            if pdef == self:
                win.addstr(16, 1, 'PLEASE THINK AGAIN... YOU ARE {}!'.format(self.country))
                win.refresh()
                curses.napms(1000)
            elif self.g.year<3 and pdef != 'barb':
                win.addstr(16, 1, 'DUE TO INTERNATIONAL TREATY, YOU CANNOT ATTACK OTHER')
                win.addstr(17, 1, 'NATIONS UNTIL THE THIRD YEAR.' )
                win.refresh()
                curses.napms(2000)
            elif pdef == 'barb' and self.g.barbarians <= 0:
                win.addstr(16, 1, 'ALL BARBARIAN LANDS HAVE BEEN SEIZED')
                win.refresh()
                curses.napms(1000)
            elif pdef != 'barb' and not pdef.alive:
                win.addstr(16, 1, 'THAT PLAYER IS NO LONGER IN THE GAME')
                win.refresh()
                curses.napms(1000)
            elif attack_num > self.nobles/4+1:
                win.addstr(16, 1, 'DUE TO A SHORTAGE OF NOBLES, YOU ARE LIMITED TO {} ATTACKS PER YEAR'.format(int(self.nobles/4+1)))
                win.refresh()
                curses.napms(1000)
            else:
                win.addstr(16, 1, 'HOW MANY SOLDIERS DO YOU WISH TO SEND? ')
                soldiers = win.getstr()
                if soldiers.isdigit():
                    soldiers = int(soldiers)
                    if soldiers > self.soldiers:
                        win.addstr(16, 1, 'THINK AGAIN... YOU ONLY HAVE {} SOLDIERS    '.format(self.soldiers))
                        win.refresh()
                        curses.napms(1000)
                    else:
                        attack_num = attack_num + 1
                        self.do_battle(pdef, soldiers)
        
    def setlevel(self):
        ''' Get level (1-3) '''
        if (self.level == 0 and
            self.markets > 7 and
            self.mills > 3 and
            self.land/self.serfs > 4.8 and
            self.serfs > 2300 and
            self.nobles >= 10):
            self.level = 1
            if self.g.verbose:
                win = new_window()
                win.addstr(3, 3, '{} has claimed the title {}!'.format(self.name, self.titles[1]))
                win.getstr()
        if (self.level == 1 and
            self.markets > 13 and
            self.mills > 5 and
            self.foundries > 0 and
            self.palace > 5 and
            self.land/self.serfs > 5 and
            self.serfs > 2600 and
            self.nobles >= 25):
            self.level = 2
            if self.g.verbose:
                win = new_window()
                win.addstr(3, 3, '{} has claimed the title {}!'.format(self.name, self.titles[2]))
                win.getstr()
        if (self.level == 2 and
            self.palace > 9 and
            self.serfs > 3100 and
            self.nobles >= 40):
            self.level = 3
            if self.g.verbose:
                win = new_window()
                win.addstr(3, 3, '{} has claimed the title {}!'.format(self.name, self.titles[3]))
                win.getstr()
        return self.level
    
    def random_death(self):
        if self.starved*random.random() > 110*random.random():
            win = new_window()
            win.addstr(3, 3, '{} HAS BEEN ASSASINATED'.format(self.full_name()))
            win.addstr(4, 3, 'BY A CRAZED MOTHER WHOSE CHILD HAD STARVED TO DEATH...')
            win.addstr(5, 3, '<ENTER>')
            self.alive = False
            win.getstr()
        elif random.random() < .01:
            win = new_window()
            msg = ['HAS BEEN ASSASSINATED BY AMBITIOUS NOBLE',
            'HAS BEEN KILLED FROM A FALL DURING THE ANNUAL FOX-HUNT.',
            'DIED OF ACUTE FOOD POISONING. THE ROYAL COOK WAS SUMMARILY EXECUTED.',
            'PASSED AWAY THIS WINTER FROM A WEAK HEART.'][random.randint(0, 3)]
            win.addstr(2, 2, 'VERY SAD NEWS...')
            win.addstr(3, 3, '{}'.format(self.full_name()))
            win.addstr(4, 3, msg)
            win.addstr(5, 3, 'THE OTHER NATION-STATES HAVE SENT REPRESENTATIVES TO THE FUNERAL.')
            win.addstr(6, 3, '<ENTER>')
            self.alive = False
            win.getstr()        
        return self.alive
        
    def plague(self):
        ''' Plague has struck... '''
        win = new_window()
        win.addstr(2, 10, 'P L A G U E   ! ! !')
        win.addstr(3, 5, 'BLACK DEATH HAS STRUCK YOUR NATION.')
        i = random.randint(0, int(self.serfs))
        self.serfs -= i
        win.addstr(4, 5, '{} SERFS DIED'.format(i))
        i = random.randint(0, int(self.merchants))
        self.merchants -= i
        win.addstr(5, 5, '{} MERCHANTS DIED'.format(i))
        i = random.randint(0, int(self.soldiers))
        self.soldiers -= i
        win.addstr(6, 5, '{} SOLDIERS DIED'.format(i))
        i = random.randint(0, int(self.nobles))
        self.nobles -= i
        win.addstr(7, 5, '{} NOBLES DIED'.format(i))
        win.addstr(9, 5, '<ENTER>')
        win.getstr()
    
    def do_grain(self):
        ''' Player distribute grain (subclass for AI) '''
        people_need, army_needs, rats = self.calc_grain()
        
        win = new_window()
        curses.curs_set(1)
        c = None  # Input character
        while c is not '\n':
            win.clear()
            win.border(0)
            win.addstr(1, 3, 'RATS ATE {0:.0f}% of the grain reserve.'.format(rats))

            win.addstr(2, 2, 'GRAIN')
            win.addstr(3, 2, 'HARVEST')
            win.addstr(4, 2, '{:,.0f}'.format(self.harvest))
            win.addstr(5, 2, 'BUSHELS')

            win.addstr(2, 17, 'GRAIN')
            win.addstr(3, 17, 'RESERVE')
            win.addstr(4, 17, '{:,.0f}'.format(self.grain))
            win.addstr(5, 17, 'BUSHELS')

            win.addstr(2, 34, 'PEOPLE')
            win.addstr(3, 34, 'REQUIRE')
            win.addstr(4, 34, '{:,.0f}'.format(people_need))
            win.addstr(5, 34, 'BUSHELS')

            win.addstr(2, 52, 'ARMY')
            win.addstr(3, 50, 'REQUIRES')
            win.addstr(4, 47, '{:>10,.0f}'.format(army_needs))
            win.addstr(5, 50, 'BUSHELS')

            win.addstr(2, 66, 'ROYAL')
            win.addstr(3, 65, 'TREASURY')
            win.addstr(4, 63, '{:>10,.0f}'.format(self.money))
            win.addstr(5, 65, self.currency)

            win.addstr(8, 1, '------- GRAIN FOR SALE:')
            win.addstr(9, 9, 'COUNTRY        BUSHELS   Price')
            line = 10
            for i, player in enumerate(self.g.players):
                if player.grainsale > 0 and player.alive:
                    win.addstr(line, 7, '{} {} \t{:,.0f} \t  {:,.2f}'.format(i+1, player.country, player.grainsale, player.grainprice))
                    line = line + 1
            if line == 10:
                win.addstr(line+2, 9, 'No grain for sale...')

            win.addstr(18, 3, '1) BUY GRAIN  2) SELL GRAIN  3) SELL LAND?')

            c = None
            while c not in ['1', '2', '3', '\n']:
                c = win.getkey(18, 46)

            if c == '1':    # Buy grain
                win.addstr(19, 2, 'FROM WHICH COUNTRY (GIVE #)? ')
                cntry = win.getkey()
                if not cntry.isdigit(): continue
                cntry = int(cntry) - 1
                if cntry < 0 or cntry > 5 or self.g.players[cntry].grainsale<=0 or not self.g.players[cntry].alive:
                    win.addstr(19, 2, 'THAT COUNTRY HAS NONE FOR SALE!')
                    win.refresh()
                    curses.napms(1000)
                    win.move(19, 2)
                    win.clrtoeol()
                elif self.g.players[cntry] is self:
                    win.addstr(19, 2, 'YOU CANNOT BUY GRAIN THAT YOU HAVE PUT ONTO THE MARKET!')
                    win.refresh()
                    curses.napms(1000)
                    win.move(19, 2)
                    win.clrtoeol()
                else:
                    win.addstr(19, 2, 'HOW MANY BUSHELS? ')
                    win.clrtoeol()
                    bushels = win.getstr()
                    if not bushels.isdigit(): continue
                    bushels = int(bushels)
                    if bushels > self.g.players[cntry].grainsale:
                        win.addstr(19, 2, 'YOU CANNOT BUY MORE GRAIN THAN THEY ARE SELLING!')
                        win.refresh()
                        curses.napms(1000)
                    elif bushels * self.g.players[cntry].grainprice > self.money:
                        win.addstr( 19, 2, 'PLEASE RECONSIDER - YOU CAN ONLY AFFORD TO BUY {:.0f} BUSHELS'.format( self.money/self.g.players[cntry].grainprice))
                        win.refresh()
                        curses.napms(1000)
                    else:
                        self.grain += bushels
                        self.money = self.money - bushels * self.g.players[cntry].grainprice
                        self.g.players[cntry].grainsale = self.g.players[cntry].grainsale - bushels
                        self.g.players[cntry].money = self.g.players[cntry].money + bushels * self.g.players[cntry].grainprice

            elif c == '2':  # Sell grain
                win.addstr(19, 2, 'HOW MANY BUSHELS DO YOU WISH TO SELL? ')
                bushels = win.getstr()
                if not bushels.isdigit(): continue
                bushels = int(bushels)
                if bushels > self.grain:
                    win.addstr(19, 2, 'PLEASE THINK AGAIN, YOU ONLY HAVE {:,.0f} BUSHELS.'.format(self.grain))
                    win.refresh()
                    curses.napms(1000)
                else:
                    win.addstr(20, 2, 'WHAT WILL BE THE PRICE PER BUSHEL? ')
                    price = win.getstr()
                    try:
                        price = float(price)
                    except:
                        continue
                    if price > 15:
                        win.addstr(20, 2, 'BE REASONABLE - EVEN GOLD COSTS LESS THAN THAT!')
                        win.refresh()
                        curses.napms(1000)
                    else:
                        self.grainprice = (self.grainprice*self.grainsale + bushels*price) / (self.grainsale+bushels)
                        self.grainsale += bushels
                        self.grain -= bushels

            elif c == '3':  # Sell land
                win.addstr(19, 2, 'THE BARBARIANS WILL GIVE YOU 2 {} PER ACRE.'.format(self.currency))
                win.addstr(20, 2, 'HOW MANY ACRES WILL YOU SELL THEM? ')
                acres = win.getstr()
                if not acres.isdigit(): continue
                acres = int(acres)
                if acres < 0: continue
                if acres > self.land*0.95:
                    win.addstr(20, 2, 'YOU MUST KEEP SOME LAND FOR THE ROYAL PALACE!')
                else:
                    self.land -= acres
                    self.money += acres*2
                    self.g.barbarians += acres

        # Give out grain
        win.addstr(18, 3, 'HOW MANY BUSHELS WILL YOU GIVE TO YOUR ARMY OF {} MEN? '.format(self.soldiers))
        army = -1
        while True:
            army = win.getstr(18, 58)
            if army == '': army=0
            if army.isdigit():
                army = int(army)
                if army > self.grain:
                    win.addstr(19, 3, 'YOU CANNOT GIVE YOUR ARMY MORE GRAIN THAN YOU HAVE!')
                else:
                    self.grain -= army
                    break
        people = self.serfs + self.merchants + self.nobles
        win.addstr(18, 3, 'HOW MANY BUSHELS WILL YOU GIVE TO YOUR {} PEOPLE?           '.format(people))
        while True:
            food = win.getstr(18, 58)
            if food == '\n': army=0
            if food.isdigit():
                food = int(food)
                if food > self.grain:
                    win.addstr(19, 3, 'BUT YOU ONLY HAVE {:.0f} BUSHELS! '.format(self.grain))
                elif food < self.grain * .1:
                    win.addstr(19, 3, 'YOU MUST RELEASE AT LEAST 10% OF THE STORED GRAIN')
                else:
                    self.grain -= food
                    break
        self.calc_population(food, people, army, people_need, army_needs)
        
    def calc_income(self):
        ''' Calculate income using weird formulas '''
        money = {}
        money['imarket'] = (self.markets * ((self.merchants+(35*random.random()))/(self.tax_s+1)*12+5))**0.9
        money['imills']  = (self.mills * (5.8*(self.harvest+250*random.random())/(self.tax_i*20+self.tax_s*40+10)+150))**.9
        money['ifound']  = (self.foundries * (self.soldiers+150*random.random()+400))**.9
        money['iship']   = (self.shipyards * (self.merchants*4+self.markets*9+self.foundries*15)*self.g.weather)**.9
        money['isold']   = -self.soldiers*8
        money['itax_c'] = self.immigrant*(40*random.random())+(40*random.random())/100*self.tax_c
        money['itax_s'] = self.tax_s/100*((self.merchants*1.8+money['imarket']*33+money['imills']*17+money['ifound']*50+money['iship']*70)**.85+self.nobles*5+self.serfs)
        money['itax_i'] = (self.tax_i/100*(self.serfs*1.3+self.nobles*145+self.merchants*39+self.markets*99+self.mills*99+self.foundries*425+self.shipyards*965))**.97
        money['total'] = int(money['imarket'] + money['imills'] + money['ifound'] + money['iship'] + money['isold'] + money['itax_c'] + money['itax_s'] + money['itax_i'])
        return money

    def do_invest(self):
        ''' Add new investments (subclass for AI) '''
        win = new_window()
        newmoney = self.calc_income()
        self.money += newmoney['total']
        people = self.serfs + self.merchants + self.nobles

        win.addstr(1, 20, 'STATE REVENUES:  TREASURY=  {0:,.0f} {1}'.format(self.money, self.currency))
        win.addstr(2, 1,  'CUSTOMS TAX    SALES TAX     INCOME TAX')
        win.addstr(4, 2,  '{}%'.format(self.tax_c))
        win.addstr(4, 17, '{}%'.format(self.tax_s))
        win.addstr(4, 32, '{}%'.format(self.tax_i))
        win.addstr(5, 2,  '{:.0f}'.format(newmoney['itax_c']))
        win.addstr(5, 17, '{:.0f}'.format(newmoney['itax_s']))
        win.addstr(5, 32, '{:.0f}'.format(newmoney['itax_i']))

        win.addstr(7, 30, 'NUMBER     ANNUAL         UNIT' )
        win.addstr(8, 3,  'INVESTMENTS                OWNED      PROFITS        COST' )
        win.addstr(9, 3,  '-----------                -----      -------        ----' )
        win.addstr(10,3,  '1) MARKET PLACES           {:<3}        {:<6.0f}         1000'.format(self.markets, newmoney['imarket']))
        win.addstr(11,3,  '2) GRAIN MILLS             {:<3}        {:<6.0f}         2000'.format(self.mills, newmoney['imills']))
        win.addstr(12,3,  '3) FOUNDRIES               {:<3}        {:<6.0f}         7000'.format(self.foundries, newmoney['ifound']))
        win.addstr(13,3,  '4) SHIPYARDS               {:<3}        {:<6.0f}         8000'.format(self.shipyards, newmoney['iship']))
        win.addstr(14,3,  '5) SOLDIERS                {:<3}        {:<6.0f}            8'.format(self.soldiers, newmoney['isold']))
        win.addstr(15,3,  '6) PALACE                  {:<3.0%}DONE                   5000'.format(self.palace/20))

        taxid = None
        while taxid is not '\n':
            win.addstr(17, 5, 'SELECT ONE TO CHANGE TAXES OR HIT <ENTER> TO CONTINUE:')
            win.addstr(19, 3, '1) CUSTOMS TAX   2) SALES TAX   3) INCOME TAX ')

            taxid = win.getkey()
            if taxid.isdigit():
                if taxid == '1':
                    win.addstr(19, 3, 'GIVE NEW CUSTOMS TAX (MAX=50%)                          ')
                    while True:
                        newtax = win.getstr(19, 45)
                        if newtax.isdigit():
                            newtax = int(newtax)
                            if newtax >= 0 and newtax < 51:
                                self.tax_c = newtax
                                win.addstr(4, 2,  '{:>2.0f}%'.format(self.tax_c))
                                break
                elif taxid == '2':
                    win.addstr(19, 3, 'GIVE NEW SALES TAX (MAX=20%)                            ')
                    while True:
                        newtax = win.getstr(19, 45)
                        if newtax.isdigit():
                            newtax = int(newtax)
                            if newtax >= 0 and newtax < 21:
                                self.tax_s = newtax
                                win.addstr(4, 17, '{:>2.0f}%'.format(self.tax_s))
                                break
                elif taxid == '3':
                    win.addstr(19, 3, 'GIVE NEW INCOME TAX (MAX=35%)                            ')
                    while True:
                        newtax = win.getstr(19, 45)
                        if newtax.isdigit():
                            newtax = int(newtax)
                            if newtax >= 0 and newtax < 36:
                                self.tax_i = newtax
                                win.addstr(4, 32, '{:>2.0f}'.format(self.tax_i))
                                break

        inv = None
        while inv is not '\n':
            win.addstr(17, 3, 'ANY NEW INVESTMENTS (GIVE #, <ENTER> TO CONTINUE)?          ')
            win.addstr(19, 3, '                                                            ')
            inv = win.getkey(17, 54)
            if not inv.isdigit() or int(inv)>6 or int(inv)<1: continue
            inv = int(inv)
            cost = [0, 1000, 2000, 7000, 8000, 8, 5000][inv]
            win.addstr(19, 3, 'HOW MANY?                                                 ')
            num = win.getstr(19, 14)
            if not num.isdigit() or int(num)<1: continue
            num = int(num)
            if self.money < cost * num:
                win.addstr(19, 3, 'THINK AGAIN...YOU ONLY HAVE {0:.0f} {1}'.format(self.money, self.currency))
                win.refresh()
                curses.napms(1000)
                continue
            elif inv == 1:  # Markets
                newmerch = random.randint(0, 7)
                if num*(num-1)*newmerch > self.serfs:
                    win.addstr(19, 3, "YOU DON'T HAVE ENOUGH SERFS TO TRAIN")
                    win.refresh()
                    curses.napms(1000)
                    continue
                self.markets += num
                self.merchants += newmerch * num
                win.addstr(10, 3, '1) MARKET PLACES           {:<3}        {:<6.0f}         1000'.format(self.markets, newmoney['imarket']))
            elif inv == 2:
                self.mills += num
                win.addstr(11, 3, '2) GRAIN MILLS             {:<3}        {:<6.0f}         2000'.format(self.mills, newmoney['imills']))
            elif inv == 3:
                self.foundries += num        
                win.addstr(12, 3, '3) FOUNDRIES               {:<3}        {:<6.0f}         7000'.format(self.foundries, newmoney['ifound']))
            elif inv == 4:
                self.shipyards += num
                win.addstr(13, 3, '4) SHIPYARDS               {:<3}        {:<6.0f}         8000'.format(self.shipyards, newmoney['iship']))
            elif inv == 5:  
                if num + self.soldiers > self.nobles*20:
                    nstr = 'NOBLE'
                    if self.nobles+.5 > 1: nstr = 'NOBLES'
                    win.addstr(19, 3, 'PLEASE THINK AGAIN...  YOU ONLY HAVE {0} {1} TO LEAD YOUR TROOPS'.format(self.nobles, nstr))
                    win.refresh()
                    curses.napms(1000)
                    continue
                elif (num + self.soldiers)/people > 0.05 + self.foundries*.015:
                    win.addstr(19, 3, 'YOU CANNOT EQUIP AND TRAIN SO MANY TROOPS, {}'.format(self.titles[self.level]))
                    win.refresh()
                    curses.napms(1000)
                    continue
                self.soldiers += num
                self.serfs -= num
                win.addstr(14, 3, '5) SOLDIERS                {:<3}        {:<6.0f}            8'.format(self.soldiers, newmoney['isold']))
            elif inv == 6:
                newnoble = random.randint(0, 4)
                if num*(num-1)*newnoble > self.serfs:
                    win.addstr(19, 3, "YOU DON'T HAVE ENOUGH SERFS TO TRAIN")
                    win.refresh()
                    curses.napms(1000)
                    continue
                self.palace += num
                self.nobles += newnoble * num
                win.addstr(15, 3, '6) PALACE                  {:<4.0%} DONE                 5000'.format(self.palace/20))
            self.money -= num * cost
            win.addstr(1, 47, '{0:>7,.0f} {1}  '.format(self.money, self.currency))
    
    def do_battle(self, pdef, soldiers):
        ''' Battle selected, run it '''
        win = new_window()
        win.addstr(4, 10, self.full_name())
        win.addstr(8, 17, 'SOLDIERS REMAINING: ')
        win.addstr(9, 5,  '{:>30}: '.format('PAGAN BARBARIANS' if pdef == 'barb' else pdef.full_name()))

        a_eff = self.armyeff      # Attacker's efficiency
        cap_land  = 0  # Captured land
        serfs = False  # Fighting serfs?
        self.soldiers -= soldiers

        if pdef == 'barb':
            d_eff = 9
            d_land = self.g.barbarians
            d_sold = int((3 * soldiers*random.random()*random.random() ) + (soldiers*(1.5)*random.random())*random.random())
            delay = 75 - soldiers - d_sold  # something for speed of battle
        else:
            d_eff = pdef.armyeff  # Defendant's efficiency
            d_sold = pdef.soldiers
            d_land = pdef.land
            delay = 75 - soldiers - d_sold  # something for speed of battle
            if d_sold < 1:
                d_sold = pdef.serfs
                serfs = True
                d_eff = 5
                delay = -1
                win.addstr(3, 2, "{}'S SERFS ARE FORCED TO DEFEND THEIR COUNTRY!".format(pdef.country))
                win.addstr(9, 5, '{:>24} SERFS: '.format(pdef.country))

        while soldiers>0 and d_sold>0:
            win.addstr(8, 39, '{:>4}'.format(soldiers))
            win.addstr(9, 39, '{:>4}'.format(d_sold))
            win.refresh()
            curses.napms(100+delay)  # Original delay was based on CPU cycles! This is a guess at what the intended speed should be
            loss = soldiers//15 + 1
            if a_eff*random.random() < d_eff*random.random():  # You lose a bit
                soldiers = max(soldiers - loss, 0)
            else:  # You win a bit
                d_sold = max(d_sold - loss, 0)
                cap_land = cap_land + loss*26*random.random() - ((loss+5)*random.random())
                cap_land = max(cap_land, 0)
            if d_land - cap_land < 1:
                # Captured all the land! Battle over.
                break
        soldiers = max(soldiers, 0)
        d_sold = max(d_sold, 0)

        win.clear()
        win.addstr(5, 25, 'b a t t l e   o v e r  ! ! !')
        if (serfs and d_sold < 1) or (d_land - cap_land < 1):
            # Killed all the serfs or all the land captured
            if pdef == 'barb':
                win.addstr(7, 10, 'ALL BARBARIAN LANDS HAVE BEEN SEIZED.')
                win.addstr(8, 10, 'THE REMAINING BARBARIANS FLED.')
                self.soldiers += soldiers # Send remaining soldiers home
                self.land += cap_land
                self.g.barbarians = 0
            else:
                win.addstr(7, 10, 'THE COUNTRY OF {} WAS OVERRUN!'.format(pdef.country))
                win.addstr(8, 10, 'ALL ENEMY NOBLES WERE SUMMARILY EXECUTED!')
                win.addstr(10, 10, 'HE REMAINING ENEMY SOLDIERS WERE IMPRISONED. ALL ENEMY SERFS')
                win.addstr(11, 10, 'HAVE PLEDGED OATHS OF FEALTY TO YOU, AND SHOULD NOW BE CONSIDERED')
                win.addstr(12, 10, 'TO BE YOUR PEOPLE TOO.  ALL ENEMY MERCHANTS FLED THE COUNTRY')
                win.addstr(13, 10, 'UNFORTUNATELY, ALL ENEMY ASSETS WERE SACKED AND')
                win.addstr(14, 10, 'DESTROYED BY YOUR REVENGEFUL ARMY')
                win.addstr(15, 10, 'IN A DRUNKEN RIOT FOLLOWING THE VICTORY CELEBRATION.  <ENTER>')
                win.getstr()

                # You get enemy assets
                self.soldiers += pdef.soldiers
                self.land += pdef.land
                pdef.land = 0
                pdef.serfs += pdef.serfs
                pdef.alive = False

        else:
            if cap_land > d_land / 3 and pdef != 'barb':
                # Captured a lot of land
                line = 8
                if pdef.serfs > 0: 
                    i = random.randint(0, int(pdef.serfs))
                    win.addstr(line, 10, '{} ENEMY SERFS WERE BEATEN AND MURDERED BY YOUR TROOPS!'.format(i))
                    pdef.serfs -= i
                    line += 1
                if pdef.markets > 0:
                    i = random.randint(0, pdef.markets)
                    win.addstr(line, 10, '{} ENEMY MARKETPLACES WERE DESTROYED'.format(i))
                    pdef.markets -= i
                    line += 1
                if pdef.grain > 0: 
                    i = random.randint(0, int(pdef.grain))
                    win.addstr(line, 10, '{} BUSHELS OF ENEMY GRAIN WERE BURNED'.format(i))
                    pdef.grain -= i
                    line += 1
                if pdef.mills > 0: 
                    i = random.randint(0, pdef.mills)
                    win.addstr(line, 10, '{} ENEMY GRAIN MILLS WERE SABOTAGED'.format(i))
                    pdef.mills -= i
                    line += 1
                if pdef.foundries > 0: 
                    i = random.randint(0, pdef.foundries)
                    win.addstr(line, 10, '{} ENEMY FOUNDRIES WERE LEVELED'.format(i))
                    pdef.foundries -= i
                    line += 1
                if pdef.shipyards > 0: 
                    i = random.randint(0, pdef.shipyards)
                    win.addstr(line, 10, '{} ENEMY SHIPYARDS WERE OVERRUN'.format(i))
                    pdef.shipyards -= i
                    line += 1
                if pdef.nobles > 0:
                    i = random.randint(0, int(pdef.nobles))
                    win.addstr(line, 10, '{} ENEMY NOBLES WERE SUMMARILY EXECUTED'.format(i))
                    pdef.nobles -= i
                    line += 1

                self.soldiers += int(soldiers)  # Send remaining soldiers home
                self.land += cap_land
                pdef.land -= cap_land
                pdef.soldiers = 0
                if serfs:
                    pdef.serfs = soldiers

            elif soldiers > 0:
                # Normal victory
                win.addstr(7, 10, 'THE FORCES OF {} WERE VICTORIOUS.'.format(self.short_name()))
                win.addstr(8, 13, '{:,.1f} ACRES WERE SEIZED.'.format(cap_land))
                self.soldiers += int(soldiers) # Send remaining soldiers home
                self.land += cap_land

                if pdef == 'barb':
                    self.g.barbarians -= cap_land
                else:
                    pdef.land -= cap_land
                    pdef.soldiers = 0
                    if serfs:
                        pdef.serfs = d_sold
            else:
                # Normal defeat
                win.addstr(7, 10, '{} WAS DEFEATED.'.format(self.short_name()))
                if cap_land > 2:
                    win.addstr(8, 7, 'IN YOUR DEFEAT YOU NEVERTHELESS MANAGED TO CAPTURE {:,.1f} ACRES.'.format(cap_land))
                else:
                    cap_land = 0
                self.land += cap_land
                if pdef == 'barb':
                    self.g.barbarians -= cap_land
                else:
                    pdef.land -= cap_land

                    if serfs:
                        pdef.serfs = d_sold
                    else:
                        pdef.soldiers = int(d_sold)

        if self.human:
            win.getstr()
        else:
            win.refresh()
            curses.napms(1000)
        

class PlayerAIClassic(Player):
    def __init__(self, name, country, titles, currency, game):
        super(PlayerAIClassic, self).__init__(name, country, titles, currency, game)
        self.human = False

    def do_grain(self):
        pass  # All processing done in do_invest
    
    def do_invest(self):
        # Find averages
        alive_players = sum([1 for x in self.g.players if x.alive])
        av_serfs = sum([x.serfs for x in self.g.players if x.alive]) / alive_players
        av_gsale = sum([x.grainsale for x in self.g.players if x.alive]) / alive_players
        av_gprice = sum([x.grainprice for x in self.g.players if x.alive]) / alive_players    
        av_money = sum([x.money for x in self.g.players if x.alive]) / alive_players
        av_merch = sum([x.merchants for x in self.g.players if x.alive]) / alive_players
        av_marks = sum([x.markets for x in self.g.players if x.alive]) / alive_players
        av_mills = sum([x.mills for x in self.g.players if x.alive]) / alive_players
        av_found = sum([x.foundries for x in self.g.players if x.alive]) / alive_players
        av_ships = sum([x.shipyards for x in self.g.players if x.alive]) / alive_players
        av_palace = sum([x.palace for x in self.g.players if x.alive]) / alive_players
        av_nobles = sum([x.nobles for x in self.g.players if x.alive]) / alive_players
        av_eff   = sum([x.armyeff for x in self.g.players if x.alive]) / alive_players

        # Randomize a bit
        av_serfs = int(av_serfs + 200*random.random() - 200*random.random())
        av_gsale = int(av_gsale + 1000*random.random() - 1000*random.random())
        av_gprice = abs(av_gprice + random.random() - random.random())
        av_money = int(av_money + 1500*random.random() - 1500*random.random())
        av_merch = int(av_merch + 25*random.random() - 25*random.random())
        av_marks = int(av_marks + 4*random.random() - 4*random.random())
        av_mills = int(av_mills + 2*random.random() - 2*random.random())
        if random.random() < .3:
            av_found = int(av_found + 2*random.random() - 2*random.random())
            av_ships  = int(av_ships  + 2*random.random() - 2*random.random()) 
            if random.random() < .5:
                av_palace = int(av_palace + 2*random.random() - 2*random.random())
                av_nobles = int(av_nobles + 2*random.random() - 2*random.random())

        self.serfs = max(self.serfs, av_serfs)
        self.money = max(self.money, av_money)
        self.merchants = max(self.merchants, av_merch)
        self.markets = max(self.markets, av_marks)
        self.mills = max(self.mills, av_mills)
        self.foundries = max(self.foundries, int(av_found))
        self.shipyard = max(self.shipyards, int(av_ships))
        self.palace = max(self.palace, int(av_palace))
        self.nobles = max(self.nobles, int(av_nobles))
        self.armyeff = max(self.armyeff, av_eff)
        self.soldiers = int(10 * self.nobles + (10 * self.nobles*random.random()))
        # Limit soldiers based on population and foundries
        while self.soldiers/av_serfs > self.foundries*.01+.05:
            self.soldiers = int(self.soldiers / 2)

        if av_gsale > self.grainsale and (9*random.random()>6):
            self.grainsale = av_gsale
            self.grainprice = av_gprice
            if self.g.weather < 3:
                self.grainprice = self.grainprice + random.random()/1.5
        self.buygrain()
                
    def buygrain(self):
        ''' Randomly buy grain '''
        c = -1
        while c < 0 or not self.g.players[c].alive or self.g.players[c] == self:
            c = random.randint(0, 5)
        if self.g.players[c].grainsale > 0:
            bushels = 100000
            while (bushels * self.g.players[c].grainprice > self.money):
                bushels = random.randint(0, int(self.g.players[c].grainsale))
                if bushels * self.g.players[c].grainprice < self.money:
                    # Buy it
                    self.g.players[c].grainsale -= bushels
                    self.g.players[c].money += bushels * self.g.players[c].grainprice
                else:
                    # Can't get an amount we can afford, give up after a while
                    if 9*random.random() < 3:
                        break
                           
    def select_battle(self):
        attack_cnt = 0
        while (self.soldiers>2 and attack_cnt==0) or (self.soldiers>30 and attack_cnt<self.nobles/4):
            alive_players = sum([1 for x in self.g.players if x.alive])
            if alive_players <= 1:
                break
            if 9*random.random() > 2:
                if self.g.year < 3:  # Only barbarians up to year 3
                    c = -1
                else:    
                    c = -3
                    while ((c < -1) or
                           (not self.g.players[c].alive) or
                           (self.g.players[c] is self) or
                           (c==-1 and self.g.barbarians<1)):
                           c = random.randint(-1, 5)

                if self.g.classic:
                    random.seed(int(self.g.weather))   # This is how original was... weirdly makes computer players all the same in earlier years.
                soldiers = random.randint(2, int(self.soldiers))
                self.do_battle('barb' if c == -1 else self.g.players[c], soldiers)
                attack_cnt = attack_cnt + 1
                           
    def setupscr(self):
        ''' Set up screen for this player '''
        curses.curs_set(0)
        self.g.scr.move(0,0)
        self.g.scr.clrtoeol()
        self.g.scr.refresh()
        win = new_window()
        win.addstr(10,10, "One moment - {}'s turn".format(self.full_name()))
        win.refresh()
        curses.napms(800)

        
class PlayerAI(PlayerAIClassic):
    def do_grain(self):
        people_need, army_needs, rats = self.calc_grain()
        people = self.serfs + self.merchants + self.nobles
        # Still sell grain based on average price, but only if we have an excess
        alive_players = sum([1 for x in self.g.players if x.alive])        
        av_gprice = sum([x.grainprice for x in self.g.players if x.alive]) / alive_players    
        av_gprice = abs(av_gprice + random.random() - random.random())
        if self.grain > (people_need + army_needs) * 1.5 and (9*random.random()>6):
            # Up to 1/2 of excess
            self.grainsale = (self.grain - (people_need+army_needs)) * random.random() * .5
            self.grainprice = av_gprice
            if self.g.weather < 3:
                self.grainprice = self.grainprice + random.random()/1.5
        self.buygrain()
        
        # Give what they need but keep at least 4% and release 10%
        food = max(self.grain*.1, min(people_need, self.grain*.96))
        self.grain -= food
        army = min(army_needs, self.grain*.9)
        self.grain -= army
        self.calc_population(food, people, army, people_need, army_needs)
        
    def do_invest(self):
        income = self.calc_income()
        self.money += income['total']
        newmarkets = newsold = newmills = newpalace = newfounds = newships = 0
        
        maxsold = min(20*self.nobles, self.serfs)
        maxnew = max(0, maxsold - self.soldiers)
        if self.soldiers < maxsold and maxnew > 0:
            newsold = random.randint(maxnew//2, maxnew)
        while (self.serfs > 0) and (newsold/self.serfs > self.foundries*.01+.05):   # limit based on population and foundries
            newsold = newsold//2
        if self.money > newsold*8:
            self.money -= newsold*8
            self.soldiers += newsold
            self.serfs -= newsold


        i = 0
        while self.money > 5000:
            # randomly select between palace, ship, and foundry
            i += 1
            z = random.choices(['palace', 'ship', 'found', 'nothing'],
                                weights=[(self.money>5000)*max(1, 10-self.palace)/5,
                                         (self.money>8000)*max(1, 5-self.shipyards)/2,
                                         (self.money>7000)*max(1, 3-self.foundries)/2,
                                          1.0])[0]
            if z == 'palace':
                newpalace +=1
                self.palace += 1
                self.money -= 5000
                self.nobles += random.randint(0, 4)
            elif z == 'ship':
                newships += 1
                self.shipyards += 1
                self.money -= 8000
            elif z == 'found':
                newfounds += 1
                self.foundries += 1
                self.money -= 7000
            elif z == 'nothing':
                break
                
        if (self.money > 2000 and
            random.random() < max(.3, .8 - self.mills/8)):
            newmills = random.randint(1, self.money // 2000)
            self.mills += newmills
            self.money -= newmills * 2000
        
        if self.money > 1000 and random.random() < max(.3, .9-self.markets/12):
            newmarkets = random.randint(1, self.money // 1000)
            newmerchants = random.randint(0, 7)
            while newmarkets > 0 and newmarkets*(newmarkets-1)*newmerchants > self.serfs:
                # Not enough serfs to train
                newmarkets -= 1
            self.markets += newmarkets
            self.money -= newmarkets * 1000
            self.merchants += newmerchants * newmarkets
                

class Game(object):
    def __init__(self, scr):
        self.scr = scr
        self.classic = '-c' in sys.argv   # Use "classic" AI
        self.verbose = '-v' in sys.argv   # Print extra stuff
        self.barbarians = 6000
        self.year = 0
        self.weather = 0
    
    def initplayers(self):
        COUNTRIES = ['AUVEYRON', 'BRITTANY', 'BAVARIA', 'QUATARA', 'BARCELONA', 'SVEALAND']
        TITLES = [
            ['CHEVALIER','PRINCE','ROI','EMPEREUR'],
            ['SIR','PRINCE','KING','EMPEROR'],
            ['RITTER','PRINZ','KONIG','KAISER'],
            ['HASID','CALIPH','SHEIK','SHAH'],
            ['CABALLERO','PRINCIPE','REY','EMPERADORE'],
            ['RIDDARE','PRINS','KUNG','KEJSARE']]  
        CURRENCY = ['FRANCS','FRANCS','MARKS','DINARS','PESETA','KRONA']
        SURNAMES = ['MONTAIGNE','ARTHUR','MUNSTER','KHOTAN','FERDINAND','HJODOLF']

        numplayers = -1
        while numplayers < 0 or numplayers > 6:
            self.scr.clear() 
            self.scr.addstr(0,0,'HOW MANY PEOPLE ARE PLAYING? ')
            numplayers = self.scr.getkey()
            if not numplayers.isdigit():
                numplayers = -1
                continue
            numplayers = int(numplayers)

            if numplayers > 6 or numplayers <0:
                self.scr.clear()
                self.scr.addstr(1, 0, 'MUST BE 0-6 PLAYERS. ')
                curses.napms(400)

        players = []
        for i in range(6):
            if i < numplayers:
                self.scr.addstr(i+2, 0, 'WHO IS THE RULER OF {}? '.format(COUNTRIES[i]))
                name = self.scr.getstr().decode(encoding="utf-8")
                players.append(Player(name, COUNTRIES[i], TITLES[i], CURRENCY[i], self))
            else:
                if self.classic:
                    players.append(PlayerAIClassic(SURNAMES[i], COUNTRIES[i], TITLES[i], CURRENCY[i], self))
                else:
                    players.append(PlayerAI(SURNAMES[i], COUNTRIES[i], TITLES[i], CURRENCY[i], self))
                    
        self.players = players

    def weathermsg(self):
        ''' Show weather message at new year '''
        WEATHER_MSG = [
            'POOR WEATHER.  NO RAIN. LOCUSTS MIGRATE',
            'EARLY FROST.  ARID CONDITIONS.',
            'FLASH FLOODS.  TOO MUCH RAIN.',
            'AVERAGE WEATHER.  GOOD YEAR.',
            'FINE WEATHER.  LONG SUMMER.',
            'FANTASTIC WEATHER!  GREAT YEAR!' ]        
        self.scr.clear()
        curses.curs_set(0)
        self.scr.addstr(0, 0, 'Year {}'.format(self.year))
        self.scr.addstr(2, 0, WEATHER_MSG[int(self.weather)])
        self.scr.refresh()
        curses.napms(1400)
        curses.curs_set(1)
        self.scr.clear()
        self.scr.addstr(curses.LINES-1, curses.COLS-10, 'Year {}'.format(self.year))
        
    def startscreen(self):
        curses.init_pair(1, curses.COLOR_WHITE, curses.COLOR_BLUE)
        curses.echo()
        curses.curs_set(0)
        self.scr.clear()
        self.scr.bkgd(' ', curses.color_pair(1))
        self.scr.border(0)    
        h, w = self.scr.getmaxyx()
        self.scr.addstr(int(h/2-1),int(w/2)-6,'E M P I R E', curses.color_pair(1))
        self.scr.refresh()
        curses.napms(2000)
        curses.curs_set(1)

    def check_gameover(self):
        if sum(x.alive for x in self.players) <= 1:
            # only one player left alive
            winner = [x for x in self.players if x.alive][0]
            win = new_window()
            win.addstr(3, 3, 'GAME OVER... {} WINS!!!'.format(winner.full_name()))
            win.addstr(5, 3, '<ENTER> TO SEE SUMMARY')
            win.getstr()
            self.show_summary()
            quit()
        elif (any(x.human for x in self.players) and
              not any(x.alive and x.human for x in self.players)):
            # All human players are dead
            win = new_window()
            win.addstr(3, 3, 'GAME OVER...')
            win.addstr(5, 3, '<ENTER> TO SEE SUMMARY')
            win.getstr()
            self.show_summary()
            quit()
    
    def show_summary(self):
        self.scr.move(0,0)
        self.scr.clrtoeol()
        self.scr.addstr(0,0, 'Summary')
        self.scr.refresh()
        curses.curs_set(0)
        win = curses.newwin(curses.LINES-2, curses.COLS-2, 1, 1)
        win.clear()
        win.bkgd(' ', curses.color_pair(1))
        win.border(0)

        win.addstr(1,1,  'NOBLES      SOLDIERS       MERCHANTS       SERFS       LAND       PALACE')
        for pid, p in enumerate(self.players):
            if p.alive:
                win.addstr(pid*2+3, 1,  p.full_name())
                win.addstr(pid*2+4, 4,  '{0:,}'.format(p.nobles))
                win.addstr(pid*2+4, 18, '{0:,}'.format(p.soldiers))
                win.addstr(pid*2+4, 33, '{0:,}'.format(p.merchants))
                win.addstr(pid*2+4, 44, '{0:,}'.format(p.serfs))
                win.addstr(pid*2+4, 55, '{0:,.1f}'.format(p.land))
                win.addstr(pid*2+4, 69, '{0:>4.0%}'.format(p.palace/20))
        win.addstr(18, 2, '<ENTER TO CONTINUE>')
        win.getstr()

        if self.verbose:
            win.clear()
            win.bkgd(' ', curses.color_pair(1))
            win.border(0)
            win.addstr(1,1,  'MARKETS  MILLS  FOUNDRIES  SHIPYARDS   MONEY  GRAIN')
            for pid, p in enumerate(self.players):
                if p.alive:
                    win.addstr(pid*2+3, 1,  p.full_name())
                    win.addstr(pid*2+4, 3,  '{0:,}'.format(p.markets))
                    win.addstr(pid*2+4, 11, '{0:,}'.format(p.mills))
                    win.addstr(pid*2+4, 20, '{0:,}'.format(p.foundries))
                    win.addstr(pid*2+4, 33, '{0:,}'.format(p.shipyards))
                    win.addstr(pid*2+4, 39, '{0:.0f}'.format(p.money))
                    win.addstr(pid*2+4, 47, '{0:.0f}'.format(p.grain))
            win.addstr(18, 2, '<ENTER TO CONTINUE>')
            win.getstr()
            win.clear()
            win.bkgd(' ', curses.color_pair(1))
            win.border(0)
            win.addstr(1,1,  'BABIES  IMMIGRANTS   DISEASE   MALNUTRITION   STARVED   CHANGE')
            for pid, p in enumerate(self.players):
                if p.alive:
                    win.addstr(pid*2+3, 1,  p.full_name())
                    win.addstr(pid*2+4, 3, '{0:,}'.format(p.babies))
                    win.addstr(pid*2+4, 12, '{0:,}'.format(p.immigrant))
                    win.addstr(pid*2+4, 24, '{0:,}'.format(p.disease))                    
                    win.addstr(pid*2+4, 35, '{0:,}'.format(p.malnut))
                    win.addstr(pid*2+4, 48, '{0:,}'.format(p.starved))
                    win.addstr(pid*2+4, 57, '{0:,}'.format(p.newpeople))
            win.addstr(18, 2, '<ENTER TO CONTINUE>')
            win.getstr()
            
            
    def mainloop(self):
        self.startscreen()
        self.initplayers()      
        while True:
            self.year += 1
            self.weather = random.random()*6
            self.weathermsg()
            if self.classic:
                random.seed(self.weather)  # How original was... ?
            for p in self.players:
                if not p.alive: continue
                p.setupscr()                    
                if (not self.classic or p.human) and random.random() < .02:   # Classic-mode computer players couldn't get this!
                    p.plague()
                p.do_grain()
                if not p.random_death():
                    self.check_gameover()
                    continue       
                p.do_invest()
                p.select_battle()
                self.check_gameover()
                level = p.setlevel()
                if level == 3:
                    win = new_window()
                    win.addstr(3, 3, 'GAME OVER... {} WINS!!!'.format(p.full_name()))
                    win.addstr(5, 3, '<ENTER> TO SEE SUMMARY')
                    win.getstr()
                    self.show_summary()
                    quit()
            self.show_summary()                        


def new_window():
    win = curses.newwin(curses.LINES-2, curses.COLS-2, 1, 1)
    win.clear()
    win.bkgd(' ', curses.color_pair(1))
    win.border(0)
    return win

                           
def main(scr):
    g = Game(scr)
    g.mainloop()


if __name__ == '__main__':
    curses.wrapper(main)
